<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-dnt library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\DoNotTrackClient;
use PhpExtended\HttpMessage\Request;
use PhpExtended\HttpMessage\Response;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class DntClientClient implements ClientInterface
{
	
	public $request;
	
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		$this->request = $request;
		
		return new Response();
	}
	
}

/**
 * DoNotTrackClientTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\DoNotTrackClient
 *
 * @internal
 *
 * @small
 */
class DoNotTrackClientTest extends TestCase
{
	
	/**
	 * The client to help.
	 *
	 * @var DntClientClient
	 */
	protected DntClientClient $_client;
	
	/**
	 * The object to test.
	 * 
	 * @var DoNotTrackClient
	 */
	protected DoNotTrackClient $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testAddHeader() : void
	{
		$response = $this->_object->sendRequest(new Request());
		$this->assertInstanceOf(ResponseInterface::class, $response);
		$this->assertNotEmpty($this->_client->request->getHeaderLine('DNT'));
	}
	
	public function testDoNothing() : void
	{
		$request = new Request();
		$request = $request->withHeader('DNT', '0');
		$response = $this->_client->sendRequest($request);
		$this->assertInstanceOf(ResponseInterface::class, $response);
		$this->assertEquals('0', $this->_client->request->getHeaderLine('DNT'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_client = new DntClientClient();
		
		$this->_object = new DoNotTrackClient($this->_client);
	}
	
}
